# General Remarks:
* All scripts are written in python 3 if not stated otherwise.
* usage from linux command line: python filename.py input1 input2 ...
* file paths and directories are specified relatively to the directory that contains the python scripts.
* Input for all scripts is based on directory names. Files within directories should be read automatically since they are specified within code according to output of preceding operation
* some functions are copy/pasted from other scripts but not actually used. They have been commented accordingly when noticed (there may be unused functions that are not marked)

# Order of operations

Run scripts in the following order so that the necessary input files exist for each operation.

### For expected observable distributions / likelihood curves:
1.  read events to CSV file: addUnnDatHistToCSV.py
2.  make histograms for expected distributions: CSVtoTree.py
3.  fit parameter c in N(dtilde) = 200 + c*dtilde^2 : PlotIntegralsSMNormCSV.py (has to be written into code of subsequent operations manually! -> variables c_oo , c_dp)
4.  make sum/difference of expected distributions: subtrAddCSV.py
5.  now likelihood curves et cetera can be computed, e.g. Poisson_NLLPlot_rebin_noBkg.py

### For likelihood fits with pseudo experiments:
1. steps 1-4 of  previous section (expected observable distributions) are required
2. generate pseudo experiments with no background (drawSample.py , **read instructions** of drawSample.py for correct input!)
3. make sum/difference of pseudo experiments: subtrAddCSVsamples_all.py
4. now fits can be performed, e.g. Poisson_Fit_Samples_new.py

### For calibration curve:
1. steps 1-3 of section expected observable distributions are required
2. generate pseudo experiments with no background (drawSample.py , **read instructions** of drawSample.py for correct input!)
3. make calibration curves (Plot_Mean_Err_SMVBF.py)

-----------------------------------------------------------------------------------------------------------------------------------------------------------

# Expected Observable Distributions

## addUnnDatHistToCSV.py

This **python 2** script reads events from .root files (NTuples), applies phase space cuts and calculates values of OO and signed delPhi with corresponding weigths w1, w2.

### Input:
* file name (for output)
* pT cut value
* mass cut value
* delta Eta cut value
* hemisphere cut (0 or 1) : turns the selection criterion of two leading jets in different hemispheres on/off (only for calculation of signed delPhi)
* partons_out cut (0,2 or 3 | 0 means no cut) turns selection criterion of 2 or 3 partons in final state on/off

### Note:
* Paths to input .root files are specified within the code and have to be edited if files are moved.

### Output:

* Two seperate .csv files for OO, delPhi respectively
* each .csv file contains a line with columns OO/delPhi; w1; w2 for every selected event


## CSVtoTree.py

this script reads the OO/delPhi values into histograms while reweighting to various dtilde values
### Input:
* directory of input .csv files (which contains seperate .csv files for OO and delPhi) as created in addUnnDatHistToCSV.py

### Note:
* list of dtilde values and binning parameters (Nbins and range) are included in the code

### Output: 
* A .root file that contains a branch for OO and delPhi respectively where the (Unnormalized) histograms for all dtilde values are saved

## PlotIntegralsSMNormCSV.py

plot Integral over expected histogram as a function of dtilde and fit parameter c in N(dtilde) = 200 + c*dtilde^2 for OO and delPhi respectively.

### Input:
* directory of .root file created by CSVtoTree.py

### Output:
* no output file: fit results are printed in terminal.

## subtrAddCSV.py

Save Sum/Difference of expected distribution and mirror image into .root File for each dtilde given in dtildelist inside code.

### Input:
* directory of histogram .root file as created in CSVtoTree.py

### Note:
* List of dtilde values included in code

### Output:
* Sum/Diff histograms are saved into input .root file

## plotBinsdtildeCSV.py

* plots for dtilde dependence of bin content / sum / diff

### Input:
* type of histogram to be examined (Integer: 0 = counts / 1 = difference / 2 = sum )
* type of normalization(Integer: 0 -> N(dtilde) 200+c*dtilde^2 , 1 -> N=200 for all dtilde)
* directory of .root file created by CSVtoTree.py

### Output:
* no output file
* plots will be displayed on screen
* parameters of linear fit will be printed in terminal

## Poisson_NLLPlot_rebin_noBkg.py

* Plot Poisson Likelihood Profile for observation = expectation (no Background)
* Compute best estimator, 1sigma and 2sigma CI

### Input:
* source directory name as in CSVtoTree.py
* Norm (set to 0 or 1: 0 = 200 signal events in SM hypothesis, 1 = 200 signal events in any hypothesis)

### Output:
* results are written into .csv file
* plots should open in new window

## Skel_NLLPlot_rebin_noBkg.py

* Plot Poisson Likelihood Profile for observation = expectation (no Background)
* Compute best estimator, 1sigma and 2sigma CI
* Input/Output analogous to Poisson_NLLPlot_rebin_noBkg.py

## Poisson_NLLPlot_SMVBF.py

* Plot Poisson Likelihood Profile for observation = expectation + generated background
* Compute best estimator, 1sigma and 2sigma CI

### Input:
* source directory name as in CSVtoTree.py
* Bkg ratio
* Norm (set to 0 or 1: 0 = 200 signal events in SM hypothesis, 1 = 200 signal events in any hypothesis (currently not working correctly))

### Output:
* results are printed in terminal in LaTex Table format
* results are written into .csv file
* plots should open in new window


-----------------------------------------------------------------------------------------------------------------------------------------------------------

# Pseudo Experiments

## drawSample.py

* this script generates pseudo experiments with an observed event number that follows a poisson distribution around the mean value 200. + c*d^2

### Input:

* directory of input .csv files (which contains seperate .csv files for OO and delPhi) as created in addUnnDatHistToCSV.py
* dtilde value

### Note:
* the respective c values for OO/delPhi respectively are obtained from PlotIntegralsSMNormCSV.py and included in code.
* currently, pseudo experiments are generated only for one dtilde value which has to be given as input. Looping over a list of dtilde values can be easily done within
code but is very time consuming. In practice, looping over a set of dtilde values was done in a linux shell script.
* for **likelihood fit**, dtilde values have to be given with two decimal places for the corresponding scripts (e.g. Poisson_Fit_Samples_new.py) to run properly. For **calibration curve** (Plot_Mean_Err_SMVBF.py), dtilde values have to be given with three decimal places. Pseudo experiments were generated seperately for the two cases and saved into different directories.

### Output:
* Subdirectory AllSamples in input directory: contains a .root file with pseudo experiment histograms for each dtilde value. 

## subtrAddCSVsamples_all.py
Save Sum/Difference of observed (pseudo experiment) distribution and mirror image into a .root File for dtilde value given as input

### Input:
* directory as created in addUnnDatHistToCSV.py
* dtilde value

### Note:
* Pseudo experiments from drawSample.py are required

### Output:
* .root file that contains Sum/Diff histograms for all pseudo experiments corresponting to the input dtilde value

## Poisson_Fit_Samples_new.py

Returns results for Poisson Fit to Samples(=Pseudo experiments) with no Background: Distribution of Estimators, 1sigma/2sigma quantiles, coverage ratio of CIs

### Input:
* source directory name as in CSVtoTree.py
* dtilde
* N_samples (number of samples to be drawn from previously generated pseudo experiments (up to 1700) )
* Normalization type (0 = expectation 200+c*dtilde^2, 1 = Normalize prediction to observation)

### Output:
* results for mean and 1sigma/2sigma percentiles of estimator distribution as well as coverage ratio of 1sigma and 2sigma CIs are written into .csv file
* histogram of estimators is written into .root file

## Skel_Fit_noBkg_new.py

Returns results for Skellam Fit to Samples(=Pseudo experiments) with no Background: Distribution of Estimators, 1sigma/2sigma quantiles, coverage ratio of CIs

### Input:
* source directory name as in CSVtoTree.py
* dtilde
* N_samples (number of samples to be drawn from previously generated pseudo experiments (up to 1700) )

### Output:
* results for mean and 1sigma/2sigma percentiles of estimator distribution as well as coverage ratio of 1sigma and 2sigma CIs are written into .csv file
* histogram of estimators is written into .root file

## Poisson_Fit_Samples_Bkg_SMVBF.py
Returns results for Poisson Fit to Samples(=Pseudo experiments) with Background: Distribution of Estimators, 1sigma/2sigma quantiles, coverage ratio of CIs
### Input:
* source directory name as in CSVtoTree.py
* dtilde
* N_samples (number of samples to be drawn from previously generated pseudo experiments (up to 1700) )
* background ratio (N_background/N_signal)

### Output:
* results for mean and 1sigma/2sigma percentiles of estimator distribution as well as coverage ratio of 1sigma and 2sigma CIs are written into .csv file
* histogram of estimators is written into .root file

## Skel_Fit_SMVBF_oo.py
Returns results for Skellam Fit to Samples(=Pseudo experiments) of OO asymmetry with Background: Distribution of Estimators, 1sigma/2sigma quantiles, coverage ratio of CIs
### Input:
* source directory name as in CSVtoTree.py
* dtilde
* N_samples (number of samples to be drawn from previously generated pseudo experiments (up to 1700) )
* background ratio (N_background/N_signal)

### Output:
* results for mean and 1sigma/2sigma percentiles of estimator distribution as well as coverage ratio of 1sigma and 2sigma CIs are written into .csv file
* histogram of estimators is written into .root file

## Skel_Fit_SMVBF_dp.py
* same code as for Skel_Fit_SMVBF_oo.py for signed delta Phi

-----------------------------------------------------------------------------------------------------------------------------------------------------------
# Calibration Curve

## Plot_Mean_Err_SMVBF.py

* Calculate and plot calibration curve.

### Note:
* Set background ratio to zero since calibration curve with background is currently not working and has to be re-implemented properly.
* consider instructions in drawSample.py for correct input of pseudo experiments.
* list of dtilde values considered for calibration curve is included in code.

### Input:
* Source directory name as in CSVtoTree.py
* output directory name
* N_Samples: Number of pseudo experiments per dtilde value
* dtilde hypothesis for calculation of CI
* background ratio (set to zero, currently not implemented properly)

### Output:
* Calibration curves are saved into .root file as TGraph objects
* results for estimators and confidence intervals are written into .csv file

